//
//  ExtractionMethods.cpp
//  StarTrackerX
//
//  Created by Carlos on 4/4/16.
//  Copyright © 2016 Carlos Granados. All rights reserved.
//

#include "ExtractionMethod.hpp"

using namespace stx;

class ExtractionMethod {

protected:
     StarTriad triad;
    
public:
    ExtractionMethod* setTriad(StarTriad triad) {
        this->triad = triad;
        return this;
    }
    
    StarTriad getTriad() {
        return this->triad;
    }
    
    std::vector<int> extract();
    
    double norm(cv::Point point) {
        return sqrt( (point.x ^ 2) + (point.y ^ 2));
    }
};