//
//  FrameData.cpp
//  StarTrackerX
//
//  Created by Carlos on 4/11/16.
//  Copyright © 2016 Carlos Granados. All rights reserved.
//

#include "header.h"

#ifndef FRAMEDATA
#define FRAMEDATA

using namespace std;

namespace stx {
    typedef struct FrameData {
        
        vector<StarTriad> triads;
        int starsFound;
        
    }FrameData;
}

#endif