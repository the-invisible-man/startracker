//
//  include.h
//  StarTrackerX
//
//  Created by Carlos on 4/10/16.
//  Copyright © 2016 Carlos Granados. All rights reserved.
//

#ifndef STRTRCK_Includes_h
#define STRTRCK_Includes_h

// Native libraries
#include <string>
#include <iostream>
#include <sstream>
#include <stdio.h>
#include <climits>

// OpenCV
#include <opencv2/opencv.hpp>

/// Startracker Components

// Structs
#include "StarTriad.cpp"
#include "FrameData.cpp"

// Utils
#include "CombinationsGenerator.h"
#include "StarVision.h"
#include "GlobalServiceProvider.h"

// Open source libs
#include "json.hpp"
#include "log.h"

// Aliases
using json = nlohmann::json;

#endif