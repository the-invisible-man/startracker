//
//  StarVision.cpp
//  StarTrackerX
//
//  Created by Carlos on 4/10/16.
//  Copyright © 2016 Carlos Granados. All rights reserved.
//

#ifndef STARVISION
#define STARVISION

using namespace std;
using namespace cv;

#include "header.h"
#include "CombinationsGenerator.h"

namespace stx {
    
    // Forward declare dependency
    class CombinationsGenerator;
    
    class StarVision {
    
        const int MAX_COMBINATIONS = 25;
        
    public:
        StarVision(CombinationsGenerator *combinationsGeneratorObj);
        StarVision * loadFile(string fileLocation);
        FrameData capture();

    private:
        CombinationsGenerator *combinationsGeneratorObj;
        Mat currentImage;
        string fileLocation;
        vector<cv::KeyPoint> findStars();
        SimpleBlobDetector::Params params;
        vector<StarTriad> buildTriads(vector<KeyPoint> keypoints);
        StarVision* loadImageMatrix(Mat image);
    };
}

#endif