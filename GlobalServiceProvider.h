//
//  ServiceProvider.hpp
//  StarTrackerX
//
//  Created by Carlos on 4/11/16.
//  Copyright © 2016 Carlos Granados. All rights reserved.
//

#ifndef ServiceProvider_h
#define ServiceProvider_h

#include "header.h"

namespace stx {
    
    // Foward declare dependencies
    class CombinationsGenerator;
    class StarVision;
    
    class GlobalServiceProvider {
    private:
        static CombinationsGenerator* combinationsGeneratorObj;
        static StarVision *starVisionObj;
    public:
        static CombinationsGenerator* makeCombinationsGenerator();
        static StarVision* makeStarVision(const std::string& fileLocation = NULL);
    };
}

#endif /* ServiceProvider_hpp */
