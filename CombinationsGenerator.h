//
//  Permutator.hpp
//  StarTrackerX
//
//  Created by Carlos on 4/10/16.
//  Copyright © 2016 Carlos Granados. All rights reserved.
//

#ifndef CombinationsGenerator_h
#define CombinationsGenerator_h

#include "header.h"

namespace stx {
    class CombinationsGenerator {
    public:
        std::vector<std::vector<int>> generate(int n, int r);
    };
}

#endif /* CombinationsGenerator_h */
