//
//  main.cpp
//  StarTrackerX
//
//  Created by Carlos on 4/2/16.
//  Copyright © 2016 Carlos Granados. All rights reserved.
//
/// TODO: Implement logger class
//

#include "header.h"

int main() {

    using namespace stx;

    FrameData data;
    StarVision * visionProcessor = GlobalServiceProvider::makeStarVision("/Users/cgranados/Code/startracker/media/summertriangle.jpg");

    // Set location of image
    data = visionProcessor->capture();

    if(data.triads.size()){
        for(int i=0; i < data.triads.size(); i++){
            cout << "Triad " << i << ": \n";
            cout << "a = (" << data.triads[i].a.x << ", " << data.triads[i].a.y << ")\n";
            cout << "b = (" << data.triads[i].b.x << ", " << data.triads[i].b.y << ")\n";
            cout << "c = (" << data.triads[i].c.x << ", " << data.triads[i].c.y << ")\n\n";
        }

        cout << "Found " << data.triads.size() << " triads in current view\n";
        cout << "Identified " << data.starsFound << " stars om frame\n";
    }else{
        cout << "Unable to resolve any triads from this image";
    }

    return 0;
}