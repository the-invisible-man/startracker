//
//  Core.hpp
//  StarTrackerX
//
//  Created by Carlos on 4/12/16.
//  Copyright © 2016 Carlos Granados. All rights reserved.
//

#ifndef Core_hpp
#define Core_hpp

#include "header.h"

namespace stx {
    
    using namespace cv;
    
    template<typename T, const &string key> struct existsInStruct {
        struct Fallback { int x; }
        struct Derived : T, Fallback {};
        
        template<typename C, C> struct Cht;
        
        template<typename C> static char (&f(ChT<int Fallback::*, &C::x>*))[1];
        template<typename C> static char (&f(...))[2];
        
        static bool const value = sizeof(f<Derived>(0)) == 2;
    };
    
}

#endif /* Core_hpp */
