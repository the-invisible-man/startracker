//
//  StarVision.cpp
//  StarTrackerX
//
//  Created by Carlos on 4/10/16.
//  Copyright © 2016 Carlos Granados. All rights reserved.
//

#include "header.h"

using namespace stx;
using namespace std;
using namespace cv;

StarVision::StarVision(CombinationsGenerator *combinationsGenerator){
    this->combinationsGeneratorObj = combinationsGenerator;
}

StarVision* StarVision::loadFile(std::string fileLocation){
    this->fileLocation = fileLocation;
    this->currentImage = imread(this->fileLocation);
    
    return this;
}

StarVision* StarVision::loadImageMatrix(Mat image){
    this->currentImage = image;
    return this;
}

FrameData StarVision::capture(){
    vector<KeyPoint> stars = this->findStars();
    FrameData output;
    
    output.starsFound = stars.size() & INT_MAX;
    output.triads = this->buildTriads(stars);
    return output;
}

vector <StarTriad>StarVision::buildTriads(std::vector<KeyPoint> keypoints){
    vector <StarTriad> output;
    int totalKeypoints = keypoints.size() & INT_MAX;
    int totalCombinations = totalKeypoints > MAX_COMBINATIONS ? MAX_COMBINATIONS : totalKeypoints;
    
    std::vector<std::vector<int>> triads = this->combinationsGeneratorObj->generate(totalCombinations, 3);
    
    // Cache triads for future fetch
    
    for(int i = 0; i < triads.size(); i++){
        StarTriad triad;
        
        //cout<< "Iterating on keypoints " << triads[i][0] <<", "<< triads[i][1] << ", " << triads[i][2] <<"\n";
        
        triad.a.x = keypoints[triads[i][0]].pt.x;
        triad.a.y = keypoints[triads[i][0]].pt.y;
        
        triad.b.x = keypoints[triads[i][1]].pt.x;
        triad.b.y = keypoints[triads[i][1]].pt.y;
        
        triad.c.x = keypoints[triads[i][2]].pt.x;
        triad.c.y = keypoints[triads[i][2]].pt.y;
        
        output.push_back(triad);
    }
    
    return output;
}

vector<cv::KeyPoint> StarVision::findStars(){
    // Some stuff we need
    vector<StarTriad> triadsFound;
    Mat img_gray;
    Mat img_keypoints;
    SimpleBlobDetector::Params params;
    vector<cv::KeyPoint> keypoints;
    vector<StarTriad> triads;
    
    // Necesasry parameters for blob analisys
    params.filterByArea = true;
    params.minArea = 0.01;
    params.maxArea = 1000;
    params.filterByColor = true;
    params.blobColor = 255;
    
    // Instantiate detector
    Ptr<cv::SimpleBlobDetector> detector = cv::SimpleBlobDetector::create(params);
    
    cvtColor(this->currentImage, img_gray, CV_RGB2GRAY);
    
    // Fix matrix size and type to allocate final image
    Mat img_bw(img_gray.size(), img_gray.type());
    
    // Apply threshhold to convert into binary image and save to new matrix
    threshold(img_gray, img_bw, 100, 255, THRESH_BINARY);
    
    // Extract cordinates of blobs at their centroids, save to keypoints variable.
    detector->detect(img_bw, keypoints);
    
    drawKeypoints(img_bw, keypoints, img_keypoints);
    
    imwrite("/Users/cgranados/Code/startracker/media/final.jpg", img_keypoints);
    
    return keypoints;
}