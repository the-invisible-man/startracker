//
//  ServiceProvider.cpp
//  StarTrackerX
//
//  Created by Carlos on 4/11/16.
//  Copyright © 2016 Carlos Granados. All rights reserved.
//

#include "GlobalServiceProvider.h"

using namespace stx;

CombinationsGenerator* GlobalServiceProvider::combinationsGeneratorObj = NULL;
StarVision* GlobalServiceProvider::starVisionObj = NULL;

CombinationsGenerator* GlobalServiceProvider::makeCombinationsGenerator(){
    if(!combinationsGeneratorObj){
        combinationsGeneratorObj = new CombinationsGenerator();
    }
    return combinationsGeneratorObj;
}

StarVision* GlobalServiceProvider::makeStarVision(const std::string& fileLocation){
    if(!starVisionObj){
        starVisionObj = new StarVision(makeCombinationsGenerator());
    }
    
    if(fileLocation.length()){
        starVisionObj->loadFile(fileLocation);
    }
    
    return starVisionObj;
}